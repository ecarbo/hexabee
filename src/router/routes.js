const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/IndexPage.vue") },
      { path: "hexagono", component: () => import("pages/HexagonoPage.vue") },
      { path: "reglas", component: () => import("pages/ReglasPage.vue") },
      { path: "abeja", component: () => import("pages/AbejaPage.vue") },
      {
        path: "escarabajo",
        component: () => import("pages/EscarabajoPage.vue"),
      },
      {
        path: "saltamontes",
        component: () => import("pages/SaltamontesPage.vue"),
      },
      { path: "arana", component: () => import("pages/AranaPage.vue") },
      { path: "hormiga", component: () => import("pages/HormigaPage.vue") },
    ],
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
